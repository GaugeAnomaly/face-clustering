#!/bin/sh
#SBATCH -t 11:00:00
#SBATCH -c 4
#SBATCH -p gpu 
#SBATCH --gres=gpu:tesla:1
module load python-3.6.0
module load cuda/8.0
module load cudnn-6.0

echo "Cluster all of it"

python serialize_features.py -b './run4/faces.db' -o ./run4/found_faces
python serialize_features.py -a -i ./fotis/data/000/0000000 -b './run4/faces.db' -o ./found_faces
python cluster_features.py -f -b './run4/faces.db'
python scoring.py -b './run4/faces.db'