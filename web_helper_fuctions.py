from shelf_updater import *

def live_get_cluster_string(cluster):
    string = ''
    with shelve.open('cluster_shelf') as db:
        string = get_cluster_string(db, cluster)
    if string is not None:
        return string
    else:
        return 'No such cluster exists'

def live_get_cluster_from_face(face_id):
    string = ''
    with shelve.open('cluster_shelf') as db:
        string = get_cluster_string(db, cluster)
    if string is not None:
        return string
    else:
        return 'No such face ID exists'

def live_get_face_image(face_id):
    return '<img src="all_faces/' + face_id + '.jpg">'

def live_get_cluster_images(cluster):
    string = ''
    array = []
    with shelve.open('cluster_shelf') as db:
        array = get_cluster_array(db, cluster)
    
    if array is None:
        return 'No such cluster exists' 
    else:
        for i in array:
            string += '<img src="static/all_faces/face_' + str(i) + '.jpg">'
        return string

def generate_cluster_link(cluster):
    array = None
    with shelve.open('cluster_shelf') as db:
        array = get_cluster_array(db, cluster)
    if array is not None:
        arr_len = str(len(array))
        what = 'images'
        if arr_len == '1':
            what = 'image'
        return '<a href="/clusters?cluster=' + cluster + '">' + cluster + ' (' + arr_len + ' ' + what + ')</a>'

def live_generate_cluster_links():
    string = ''
    with shelve.open('cluster_shelf') as db:
        for i in db:
            string += generate_cluster_link(i)
            string += '<br />'
    return string