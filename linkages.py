import numpy as np
from scipy.sparse import lil_matrix

def calculate_centroid_dist(points1, points2):
    x = points1.mean(axis=0) - points2.mean(axis=0)
    return np.sqrt(x.dot(x))

def calculate_avg_dist(pairwise_dists_xy):
    return pairwise_dists_xy.mean()

def get_pairwise_dists_xy(X, Y):
    matrix = np.empty([len(X), len(Y)])
    for i, x in enumerate(X):
        for j, y in enumerate(Y):
            delta = x - y
            d = np.sqrt(np.dot(delta, delta))
            matrix[i, j] = d
    return matrix

def get_pairwise_dists_xx(X):
    matrix = np.empty([len(X)-1, len(X)-1])
    unvisited = list(range(len(X)))
    while len(unvisited) > 0:
        i = unvisited.pop(0)
        for j in unvisited:
            delta = X[i] - X[j]
            matrix[i, j] = np.sqrt(delta.dot(delta))
    return matrix

def calculate_ward_dist(pairwise_dists_xx,pairwise_dists_yy, pairwise_dists_xy):
    pass

def list_closest_clusters(dist_m, label):
    return np.argsort(dist_m[label])[1:]

def get_dict_from_labels(embeddings, labels):
    d = {}
    for i in set(labels):
        ids = np.nonzero(labels == i)[0]
        d[i] = embeddings[ids]
    return d
        

def cluster_distance_matrix(embeddings, labels): # cluster id to points array dict
    ltp = get_dict_from_labels(np.asarray(embeddings), labels)
    unvisited = list(ltp.keys())
    #matrix = lil_matrix((len(unvisited)-1,len(unvisited)-1))
    matrix = np.zeros((len(unvisited),len(unvisited)))
    for i in ltp:
        for j in unvisited:
            if i == j:
                continue
            pairwise_dists = get_pairwise_dists_xy(ltp[i], ltp[j])
            # calculate distance between them
            dist = calculate_avg_dist(pairwise_dists)
            matrix[i,j] = dist
            matrix[j,i] = dist
            # matrix = calculate_ward_dist(pairwise_dists)
            # store result to matrix
        unvisited.remove(i)
    return matrix


def cluster_distance_matrix2(embeddings, labels): # cluster id to points array dict
    ltp = get_dict_from_labels(np.asarray(embeddings), labels)
    unvisited = list(ltp.keys())
    #matrix = lil_matrix((len(unvisited)-1,len(unvisited)-1))
    matrix = np.zeros((len(unvisited),len(unvisited)))
    for i in ltp:
        for j in unvisited:
            if i == j:
                continue
            # calculate distance between them
            dist = calculate_centroid_dist(ltp[i], ltp[j])
            matrix[i,j] = dist
            matrix[j,i] = dist
            # matrix = calculate_ward_dist(pairwise_dists)
            # store result to matrix
        unvisited.remove(i)
    return matrix
