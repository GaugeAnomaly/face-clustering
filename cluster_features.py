from sklearn.cluster import DBSCAN, linkage_tree
import numpy as np
from face_models import mod_sqlite3
from sklearn.neighbors import NearestNeighbors
from scipy.sparse.csgraph import connected_components
from linkages import *
from time import perf_counter
import argparse

def main(args):
    con = mod_sqlite3.connect(args.data_base, detect_types=mod_sqlite3.PARSE_DECLTYPES)
    descriptors, = zip(*con.execute('select embedding from face'))

    def clustering(descriptors, r=0.31):
        return DBSCAN(min_samples=1, eps=r, n_jobs=4).fit_predict(descriptors)

    descriptors = [d.vector for d in descriptors]
    # n, labels2 = connected_components(kernel(descriptors, descriptors))

    start = perf_counter()

    # Now let's cluster the faces.  
    labels = clustering(descriptors, args.radius)
    print("Time taken for clustering: {}".format(perf_counter()-start))
    num_classes = len(set(labels))
    print("Number of clusters: {}".format(num_classes))
    print("Number of faces in total: {}".format(len(descriptors)))

    if not args.forbid:
        start = perf_counter()
        # m = cluster_distance_matrix(descriptors, labels)
        print("Time taken for calculating the distance matrix: {}".format(perf_counter()-start))
    # m2 = cluster_distance_matrix2(descriptors, labels)
    # np.save('dist_m', m)
    # print(m)
    # print(m2)
    # print(list_closest_clusters(m, 1))

    for i in range(num_classes):
        ids = np.nonzero(labels == i)[0]+1
        var = str(tuple(ids)) if len(ids) > 1 else '('+ str(ids[0]) +')'
        prep = 'update face set cluster_id = ? where rowid in '+ var
        con.execute(prep, [i])

    con.commit()
    con.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--data_base', help='The SQLite database file to store information', default='./face_db.db')
    parser.add_argument('-r', '--radius', help='The DBSCAN radius to use', default=0.4, type=float)
    args = parser.parse_args()
    main(args)
