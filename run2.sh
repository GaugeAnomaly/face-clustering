#!/bin/sh
#SBATCH -t 9:00:00
#SBATCH -c 4
#SBATCH -p main
module load python-3.6.0

echo "Serializing into " "face2_db.db"
python serialize_features.py -b 'face2_db.db' -o 'found_faces2'
python serialize_features.py -a -i ./fotis/isikud/000/0000000 -b 'face2_db.db' -o 'found_faces2'

for var in "$@"
do
    echo "Get results with radius" $var

    python cluster_features.py -b 'face2_db.db' -r $var
    python scoring.py -b 'face2_db.db'
done

echo "DONE!!!"