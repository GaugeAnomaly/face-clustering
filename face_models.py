import sqlite3

class Face():
    def __init__(self, x, y, z, w):
        self.x, self.y, self.z, self.w = x, y, z, w
    
    def __init__(self, drect):
        self.x, self.y, self.z, self.w = drect.left(), drect.top(),drect.right(), drect.bottom()

    def __conform__(self, protocol):
        if protocol is sqlite3.PrepareProtocol:
            return ("%f;%f;%f;%f" % (self.x, self.y, self.z, self.w)).encode('ascii')

    @staticmethod
    def convert(s):
        x, y, z, w = list(map(float, s.split(b";")))
        return Face(x, y, z, w)

class Embedding():
    def __init__(self, vector):
        self.vector = vector

    def __conform__(self, protocol):
        if protocol is sqlite3.PrepareProtocol:
            return ((127*"%f;" + "%f") % tuple(self.vector)).encode('ascii')
    @staticmethod
    def convert(s):
        vector = list(map(float, s.split(b";")))
        return Embedding(vector)

mod_sqlite3 = sqlite3
mod_sqlite3.register_converter("face_pos_t", Face.convert)
mod_sqlite3.register_converter("emb_t", Embedding.convert)