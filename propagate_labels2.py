from sklearn.semi_supervised.label_propagation import LabelPropagation as LP
from face_models import mod_sqlite3
import argparse

def main(args):
    con = mod_sqlite3.connect(args.data_base, detect_types=mod_sqlite3.PARSE_DECLTYPES)
    descriptors, = zip(*con.execute('select embedding from face'))

    con.commit()
    con.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--data_base', help='The SQLite database file to store information', default='./face_db.db')
    args = parser.parse_args()
    main(args)
