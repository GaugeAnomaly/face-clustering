#!/usr/bin/python
# The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
#
#   This example shows how to use dlib's face recognition tool for clustering using chinese_whispers.
#   This is useful when you have a collection of photographs which you know are linked to
#   a particular person, but the person may be photographed with multiple other people.
#   In this example, we assume the largest cluster will contain photos of the common person in the
#   collection of photographs. Then, we save extracted images of the face in the largest cluster in
#   a 150x150 px format which is suitable for jittering and loading to perform metric learning (as shown
#   in the dnn_metric_learning_on_images_ex.cpp example.
#   https://github.com/davisking/dlib/blob/master/examples/dnn_metric_learning_on_images_ex.cpp
#
# COMPILING/INSTALLING THE DLIB PYTHON INTERFACE
#   You can install dlib using the command:
#       pip install dlib
#
#   Alternatively, if you want to compile dlib yourself then go into the dlib
#   root folder and run:
#       python setup.py install
#   or
#       python setup.py install --yes USE_AVX_INSTRUCTIONS
#   if you have a CPU that supports AVX instructions, since this makes some
#   things run faster.  This code will also use CUDA if you have CUDA and cuDNN
#   installed.
#
#   Compiling dlib should work on any operating system so long as you have
#   CMake and boost-python installed.  On Ubuntu, this can be done easily by
#   running the command:
#       sudo apt-get install libboost-python-dev cmake
#
#   Also note that this example requires scikit-image which can be installed
#   via the command:
#       pip install scikit-image
#   Or downloaded from http://scikit-image.org/download.html. 

import sys
import os
import glob
import dlib
from skimage import io
import imageio
import cv2

def false_positives(indicies, file_name):
    in_file = []
    fp_count = 0
    with open(file_name) as f:
        for face in f:
            face.strip()
            in_file.append(int(face[5:]))
    for i in indicies:
        if i not in in_file:
            fp_count += 1
    return fp_count

if len(sys.argv) != 6:
    print(
        "Call this program like this:\n"
        "   ./face_clustering.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces output_folder /path/to/detector\n"
        "You can download a trained facial shape predictor and recognition model from:\n"
        "    http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2\n"
        "    http://dlib.net/files/dlib_face_recognition_resnet_model_v1.dat.bz2")
    exit()

predictor_path = sys.argv[1]
face_rec_model_path = sys.argv[2]
faces_folder_path = sys.argv[3]
output_folder_path = sys.argv[4]
detector_path = sys.argv[5]

# Load all the models we need: a detector to find the faces, a shape predictor
# to find face landmarks so we can precisely localize the face, and finally the
# face recognition model.
# detector = dlib.get_frontal_face_detector()
detector = dlib.cnn_face_detection_model_v1(detector_path)
sp = dlib.shape_predictor(predictor_path)
facerec = dlib.face_recognition_model_v1(face_rec_model_path)

descriptors = []
images = []

# Now find all the faces and compute 128D face descriptors for each face.
for f in sorted(glob.glob(os.path.join(faces_folder_path, "*.jpg"))):
    print("Processing file: {}".format(f))
    #img = io.imread(f)
    img = imageio.imread(f)
    if len(img.shape) == 2:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Ask the detector to find the bounding boxes of each face. The 1 in the
    # second argument indicates that we should upsample the image 1 time. This
    # will make everything bigger and allow us to detect more faces.
    mmdets = detector(img, 1)
    dets = dlib.rectangles()
    dets.extend([d.rect for d in mmdets])
    print("Number of faces detected: {}".format(len(dets)))

    # Now process each face we found.
    for k, d in enumerate(dets):
        # Get the landmarks/parts for the face in box d.
        shape = sp(img, d)

        # Compute the 128D vector that describes the face in img identified by
        # shape.  
        try:
            face_descriptor = facerec.compute_face_descriptor(img, shape)
            descriptors.append(face_descriptor)
            images.append((img, shape))
        except:
            print("Failed with image: ", f)
            print("Image Shape: ", img.shape)

# Now let's cluster the faces.  
labels = dlib.chinese_whispers_clustering(descriptors, 0.6)
num_classes = len(set(labels))
print("Number of clusters: {}".format(num_classes))
print("Number of faces in total: {}".format(len(images)))

# Find biggest class
biggest_class = None
biggest_class_length = 0
for i in range(0, num_classes):
    class_length = len([label for label in labels if label == i])
    print("Cluster ID {} contains {} members".format(i, class_length))
    if class_length > biggest_class_length:
        biggest_class_length = class_length
        biggest_class = i

print("Biggest cluster id number: {}".format(biggest_class))
print("Number of faces in biggest cluster: {}".format(biggest_class_length))

# Find indicies for all classes
index_vectors = []
for i in range(num_classes):
    indices = []
    for j, label in enumerate(labels):
        if label == i:
            indices.append(j)
    index_vectors.append(indices)

print("Indices of images in the biggest cluster: {}".format(str(index_vectors[biggest_class])))
print("False positives: {}".format(false_positives(index_vectors[0], 'truth_table.txt')))

for i in range(num_classes):
    class_folder_path = os.path.join(output_folder_path, str(i))
    # Ensure output directory exists
    if not os.path.isdir(class_folder_path):
        os.makedirs(class_folder_path)

    # Save the extracted faces
    print("Saving faces in cluster to output folder...")
    for j, index in enumerate(index_vectors[i]):
        img, shape = images[index]
        file_path = os.path.join(class_folder_path, "face_" + str(index))
        dlib.save_face_chip(img, shape, file_path)