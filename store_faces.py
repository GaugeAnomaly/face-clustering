import sys, os, dlib, pickle, shutil, shelve
from sklearn.cluster import AffinityPropagation, DBSCAN, MeanShift
import numpy as np

def labels(descriptors):
    global algorithm

    algorithm = DBSCAN(min_samples=1, eps=0.4, n_jobs=4)
    return algorithm.fit_predict(descriptors)

# if len(sys.argv) != 2:
#     print("You need to specify the output folder")
#     exit()

# output_folder_path = sys.argv[1]
output_folder_path = 'all_faces/'
if os.path.isdir(output_folder_path):
    shutil.rmtree(output_folder_path)

with open("descriptors_pickled.p", 'rb') as f:
    descriptors = pickle.load(f)
with open("images_pickled.p", 'rb') as f:
    images = pickle.load(f)

# Now let's cluster the faces.  
labels = labels(descriptors)
num_classes = len(set(labels))
print("Clustering algorithm used:", algorithm)
print("Number of clusters: {}".format(num_classes))
print("Number of faces in total: {}".format(len(images)))

print("Saving faces in clusters to output folders...")
for i in range(num_classes):
    class_folder_path = output_folder_path
    # Ensure output directory exists
    if not os.path.isdir(class_folder_path):
        os.makedirs(class_folder_path)

    # Save the extracted faces
    for index in np.nonzero(labels == i)[0]:
        img, shape = images[index]
        file_path = os.path.join(class_folder_path, "face_" + str(index))
        dlib.save_face_chip(img, shape, file_path)