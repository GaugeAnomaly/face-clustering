from pathlib import Path
from itertools import combinations
from sklearn.metrics.cluster import adjusted_rand_score as ars, adjusted_mutual_info_score as ami, completeness_score, homogeneity_score, v_measure_score
from face_models import mod_sqlite3
import argparse
# from memory_profiler import profile

def get_num(file):
    return int(file.stem[5:])

def list_of_files(folder):
    return sorted((Path(folder).glob("**/*.jpg")), key=get_num)

def files_to_inds(files):
    return [int(i.parts[-2]) for i in files]

def pair_comb_sameness(files):
    return [i[0].parts[-2] == i[1].parts[-2] for i in combinations(files, 2)]

def pair_comb_sameness_db(files):
    return [i[0] == i[1] for i in combinations(files, 2)]

def true_pos(same_true, same_pred):
    return len([i for i in zip(same_true, same_pred) if i[0] == i[1] == True])

def true_neg(same_true, same_pred):
    return len([i for i in zip(same_true, same_pred) if i[0] == i[1] == False])

def false_pos(same_true, same_pred):
    return len([i for i in zip(same_true, same_pred) if i[0] == False and i[1] == True])

def false_neg(same_true, same_pred):
    return len([i for i in zip(same_true, same_pred) if i[0] == True and i[1] == False])

def print_discrepancies(folder1, db_faces):
    if len(folder1) == 0 or len(db_faces) == 0:
        if len(folder1) == 0:
            print('Folder #1 is empty!')
        if len(db_faces) == 0:
            print('Database is empty!')
        exit()
    disc = False
    # print(files_to_inds(folder1))
    # print(files_to_inds(folder2))
    print('Num of files in', folder1[0].parts[-3], 'is', len(folder1))
    # print('Num of files in', folder2[0].parts[-3], 'is', len(folder2))
    print('Num of files in face_db.db is', len(db_faces))
    i1 = 0
    i2 = 0
    for i in range(len(max([folder1, db_faces], key=len))):
        if i1 >= len(folder1) or get_num(folder1[i1]) != i:
            print('Index', i, 'not found in', folder1[0].parts[-3])
            if i1 < len(folder1): i1 -= 1
            disc = True
        if i2 >= len(db_faces) or db_faces[i2]-1 != i:
            print('Index', i, 'not found in database')
            if i2 < len(db_faces): i2 -= 1
            disc = True
        i1 += 1
        i2 += 1
    if disc:
        exit()


def main(args):
    con = mod_sqlite3.connect(args.data_base, detect_types=mod_sqlite3.PARSE_DECLTYPES)
    test_count = 269
    ids, labels = list(zip(*con.execute('select rowid, cluster_id from face limit ?', [test_count])))

    truefaces = list_of_files(args.true_faces)

    print_discrepancies(truefaces, ids)

    indecies_true = files_to_inds(truefaces)
    indecies_pred = labels

    print('Adjusted Rand index:', ars(indecies_true, indecies_pred))
    print('Adjusted Mutual Information:', ami(indecies_true, indecies_pred))
    print('Completeness (recall):', completeness_score(indecies_true, indecies_pred))
    print('Homogeneity (precision):', homogeneity_score(indecies_true, indecies_pred))
    print('V-score: ', v_measure_score(indecies_true, indecies_pred))

# same_true = pair_comb_sameness(truefaces)
# same_pred = pair_comb_sameness_db(predicted)
# total = len(same_true)

# true_pos_num = true_pos(same_true, same_pred)
# true_neg_num = true_neg(same_true, same_pred)
# false_pos_num = false_pos(same_true, same_pred)
# false_neg_num = false_neg(same_true, same_pred)

# print('True positives:', true_pos_num, true_pos_num/total, '%')
# print('True negatives:', true_neg_num, true_neg_num/total, '%')
# print('False positives:', false_pos_num, false_pos_num/total, '%')
# print('False negatives:', false_neg_num, false_neg_num/total, '%')
# print('Trues:', true_pos_num + true_neg_num, (true_pos_num + true_neg_num)/total, '%')
# print(list_of_files('truefaces'))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--data_base', help='The SQLite database file to store information', default='./face_db.db')
    parser.add_argument('-f', '--true_faces', help='Path to the ground truth folder', default='./truefaces')
    args = parser.parse_args()
    main(args)
