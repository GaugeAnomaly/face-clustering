import sys
import os
import glob
import dlib
import argparse
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import shutil
from scipy.misc import imread
from time import perf_counter


def main(args):
    # Load all the models we need: a detector to find the faces, a shape predictor
    # to find face landmarks so we can precisely localize the face, and finally the
    # face recognition model.
    detector = dlib.cnn_face_detection_model_v1(args.detector)
    sp = dlib.shape_predictor(args.predictor)
    facerec = dlib.face_recognition_model_v1(args.recognizer)

    descriptors = []
    positions = []
    files_with_faces = []
    chips = []

    if not args.append:
        if os.path.isdir(args.all_faces_output):
            shutil.rmtree(args.all_faces_output)
        os.makedirs(args.all_faces_output)

    f = args.input
    
    img = imread(f, mode='RGB')

    # Ask the detector to find the bounding boxes of each face. The 1 in the
    # second argument indicates that we should upsample the image 1 time. This
    # will make everything bigger and allow us to detect more faces.
    mmdets = detector(img, 1)
    dets = dlib.rectangles()
    dets.extend([d.rect for d in mmdets])
    fig, ax = plt.subplots()
    ax.axis('off')
    ax.imshow(img)
    for k, d in enumerate(dets):
        shape = sp(img, d)
        rect = patches.Rectangle((d.left(), d.top()), d.width(), d.height(), fill=False, edgecolor='g', linewidth=2)
        ax.add_patch(rect)
        for i in range(5):
            plt.scatter([shape.part(i).x],[shape.part(i).y], s=2, c='r')
        file_path = os.path.join(args.all_faces_output, 'face_' + str(k))
        dlib.save_face_chip(img, shape, file_path)
        face_descriptor = facerec.compute_face_descriptor(img, shape)
    plt.savefig('image.png')    
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--append', help='Whether to append new data or rewrite existing data', action="store_true")
    parser.add_argument('-p', '--predictor', help='Path to the landmark predictor model', default='./dats/shape_predictor_5_face_landmarks.dat')
    parser.add_argument('-r', '--recognizer', help='Path to the face recognition model', default='./dats/dlib_face_recognition_resnet_model_v1.dat')
    parser.add_argument('-i', '--input', help='Input file containing images', default='./fotis/meri/efa0001_000_0000000_254712_f.jpg')
    parser.add_argument('-d', '--detector', help='Path to the face detector model', default='./dats/mmod_human_face_detector.dat')
    parser.add_argument('-o', '--all_faces_output', help='Path to new folder where to save all face chips', default='./found_faces')
    args = parser.parse_args()
    main(args)
