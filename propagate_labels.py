from sklearn.semi_supervised.label_propagation import LabelPropagation, LabelSpreading
from sklearn.cluster import DBSCAN
import numpy as np
import pickle

labels = { # label dictionary, index to label ID
    0: 1,
    1: -2,
    7: 456789,
    9: 3
}

def split_input_labels(X):
    return DBSCAN(min_samples=1, eps=0.4, n_jobs=4).fit_predict(X)

def print_distribution(dist, classes):
    for i, d in enumerate(dist):
        print("Distribution {}:\t".format(i), end='')
        for j, c in enumerate(classes):
            print("{} {:.2f}% ".format(c, 100*d[j]), end='')
        print()

def propagate_labels(X, kernel):
    n_faces = len(X) # total nuber of faces

    lp = LabelSpreading(kernel=kernel, n_jobs=-1)

    y = np.array([-1 if i not in labels else labels[i] for i in range(n_faces)])

    lp.fit(X, y)
    print_distribution(lp.label_distributions_, lp.classes_)

# TODO: return transductions
def propagate_splitted_labels(X):
    n_faces = len(X) # total nuber of faces

    splitted_labels = split_input_labels(X)
    splits = np.unique(splitted_labels)

    lp = LabelPropagation(gamma=0.25)

    all_labels = np.array([-1 if i not in labels else labels[i] for i in range(n_faces)])
    # print('all_labels', all_labels)

    for split in splits:
        print('Split', split)
        indices = np.nonzero(splitted_labels == split)[0]
        y = all_labels[indices]
        if not any(y != -1):
            continue
        X = X[indices]
        a = [1, 2, 3]
        lp.fit(X, y)
        print_distribution(lp.label_distributions_, lp.classes_)

with open("descriptors_pickled.p", 'rb') as f:
    descriptors = pickle.load(f)
    descriptors = np.array(descriptors)


# propagated_labels = propagate_labels(descriptors)

############# TESTING

from sklearn.neighbors import kneighbors_graph, radius_neighbors_graph, NearestNeighbors
from sys import getsizeof

def kernel(X, y, gamma=3):
    # rg = kneighbors_graph(descriptors, 240)
    nn = NearestNeighbors(radius=0.74, n_neighbors=240).fit(X) # 0.74
    rg = nn.radius_neighbors_graph(y, mode='distance')
    print('rg', rg)
    rg *= -gamma
    np.exp(rg.data, rg.data)
    return rg

# print(kernel(descriptors, descriptors))
# print()
# print(kernel(descriptors, descriptors).toarray())
# print()
# normalizer1 = kernel(descriptors, descriptors).toarray().sum(axis=0)
# normalizer2 = kernel(descriptors, descriptors).sum(axis=0)
# print('Normalizer1', normalizer1.shape)
# print()
# print('Normalizer2', normalizer2.shape)
# print()
# print('arr1',np.array(normalizer1))
# print('arr2',np.array(normalizer2))
# print('diag1', normalizer1[:, np.newaxis])
# print()
# print('diag2',np.diag(np.array(normalizer2)))

propagate_labels(descriptors, kernel)


# kg = kneighbors_graph(descriptors, 9)
# rg = radius_neighbors_graph(descriptors, 0.4, 'distance')

# print('knn:', kg)
# print('knn array size', getsizeof(kg.toarray()))
# print('knn size:', getsizeof(kg))
# print('rnn:', kernel(descriptors, descriptors) )
# print('rnn size:', getsizeof(rg))
# print('aff m', kernel(descriptors, descriptors))
# print('aff m size', getsizeof(kernel(descriptors, descriptors)))





#####################
