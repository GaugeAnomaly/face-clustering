import numpy as np
import matplotlib.pyplot as plt
from sklearn.semi_supervised import label_propagation
from sklearn.datasets import make_circles, make_blobs

from sklearn.neighbors import NearestNeighbors

def create_plot(label_spread):
    # generate ring with inner box
    n_samples = 200
    X, y = make_circles(n_samples=n_samples, shuffle=True, random_state=42)
    X1, y1 = make_blobs(n_samples=n_samples, shuffle=True, random_state=42, centers=10)
    outer, inner = 1, 2
    labels = -np.ones(n_samples)

    labels[0] = outer
    labels[-1] = inner

    # labels[0] = outer
    # labels[50] = inner

    # labels = labels[::-1]
    # X = X[::-1]
    # #############################################################################
    # Learn with LabelSpreading

    label_spread.fit(X1, labels)

    # #############################################################################
    # Plot output labels
   
    # output_labels = label_spread.transduction_
    # print(label_spread.label_distributions_)
    # plt.figure(figsize=(8.5, 4))
    # plt.subplot(1, 2, 1)
    # plt.scatter(X[labels == outer, 0], X[labels == outer, 1], color='navy',
    #             marker='s', lw=0, label="outer labeled", s=10)
    # plt.scatter(X[labels == inner, 0], X[labels == inner, 1], color='c',
    #             marker='s', lw=0, label='inner labeled', s=10)
    # plt.scatter(X[labels == -1, 0], X[labels == -1, 1], color='darkorange',
    #             marker='.', label='unlabeled')
    # plt.legend(scatterpoints=1, shadow=False, loc='upper right')
    # plt.title("Raw data (2 classes=outer and inner)")

    # plt.subplot(1, 2, 2)
    # output_label_array = np.asarray(output_labels)
    # outer_numbers = np.where(output_label_array == outer)[0]
    # inner_numbers = np.where(output_label_array == inner)[0]
    # plt.scatter(X[outer_numbers, 0], X[outer_numbers, 1], color='navy',
    #             marker='s', lw=0, s=10, label="outer learned")
    # plt.scatter(X[inner_numbers, 0], X[inner_numbers, 1], color='c',
    #             marker='s', lw=0, s=10, label="inner learned")
    # plt.legend(scatterpoints=1, shadow=False, loc='upper right')
    # plt.title("Labels learned with Label Spreading (KNN, a="+str(label_spread.alpha)+")")

    # plt.subplots_adjust(left=0.07, bottom=0.07, right=0.93, top=0.92)
    # plt.show()


    # #############################################################################
    # Plot output labels
    output_labels = label_spread.transduction_
    print(label_spread.label_distributions_)
    plt.figure(figsize=(8.5, 4))
    plt.subplot(1, 2, 1)
    plt.scatter(X1[labels == outer, 0], X1[labels == outer, 1], color='navy',
                marker='s', lw=0, label="outer labeled", s=10)
    plt.scatter(X1[labels == inner, 0], X1[labels == inner, 1], color='c',
                marker='s', lw=0, label='inner labeled', s=10)
    plt.scatter(X1[labels == -1, 0], X1[labels == -1, 1], color='darkorange',
                marker='.', label='unlabeled')
    plt.legend(scatterpoints=1, shadow=False, loc='upper right')
    plt.title("Raw data (2 classes=outer and inner)")

    plt.subplot(1, 2, 2)
    output_label_array = np.asarray(output_labels)
    outer_numbers = np.where(output_label_array == outer)[0]
    inner_numbers = np.where(output_label_array == inner)[0]
    plt.scatter(X1[outer_numbers, 0], X1[outer_numbers, 1], color='navy',
                marker='s', lw=0, s=10, label="outer learned")
    plt.scatter(X1[inner_numbers, 0], X1[inner_numbers, 1], color='c',
                marker='s', lw=0, s=10, label="inner learned")
    plt.legend(scatterpoints=1, shadow=False, loc='upper right')
    plt.title("Labels learned with Label Spreading (KNN, a="+str(label_spread.alpha)+")")

    plt.subplots_adjust(left=0.07, bottom=0.07, right=0.93, top=0.92)
    plt.show()

def kernel(X, y, gamma=120): #120
    # rg = kneighbors_graph(descriptors, 240)
    nn = NearestNeighbors(radius=2).fit(X) # 0.19
    rg = nn.radius_neighbors_graph(y, mode='distance')
    rg *= -gamma
    np.exp(rg.data, rg.data)
    return rg

# label_spread = label_propagation.LabelPropagation(kernel=kernel, n_neighbors=30)
# create_plot(label_spread)

for a in range(9):
    a = (a+1)/10
    label_spread = label_propagation.LabelSpreading(kernel=kernel, alpha=a)
    create_plot(label_spread)