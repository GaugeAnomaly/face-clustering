#!/bin/sh
#SBATCH -t 9:00:00
#SBATCH -c 4
#SBATCH -p gpu 
#SBATCH --gres=gpu:tesla:1
module load python-3.6.0
module load cuda/8.0
module load cudnn-6.0

# kernprof -lv serialize_features_d.py
# kernprof -lv cluster_features_d.py
# echo "Start profiling with 2731 images"
# kernprof -lv serialize_features_d.py -a -i ./fotis/isikud/000/0000000
# kernprof -lv cluster_features_d.py
# echo "Lots of images"
# kernprof -lv serialize_features_d.py -a -i ./fotis/data/001/0000000
# kernprof -lv cluster_features_d.py

# python serialize_features.py
python -m memory_profiler serialize_features_d.py
python -m memory_profiler cluster_features_d.py
python -m memory_profiler serialize_features_d.py -a -i ./fotis/isikud/000/0000000
python -m memory_profiler cluster_features_d.py
python -m memory_profiler serialize_features_d.py -a -i ./fotis/data/001/0000000
python -m memory_profiler cluster_features_d.py
# python -m memory_profiler cluster_features_d.py

echo "Finished profiling"
