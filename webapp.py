from bottle import route, run, template, request
import webbrowser
import web_helper_fuctions as hf
from bottle import static_file

@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='.')

@route('/')
def index():
    return template('index', extra=hf.live_generate_cluster_links())

@route('/clusters', method='GET')
def cluster():
    return hf.live_get_cluster_images(request.GET['cluster'])

webbrowser.open_new_tab('http://localhost:8080')
run(host='localhost', port=8080)
