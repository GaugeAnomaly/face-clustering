Serializing into  ./run3/face2_db.db
Total time: 335.71016067638993
Processed 240 files in total
Total time: 3423.2488744417205
Processed 2491 files in total
Total time: 13302.462272657081
Processed 9722 files in total
Get results with radius 0.31
Time taken for clustering: 32.31569950282574
Number of clusters: 7443
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.580454409122467e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.0524241355228
Adjusted Mutual Information: 0.118779524096
Completeness (recall): 0.54048367447
Homogeneity (precision): 0.989091386213
V-score:  0.699001651568
Get results with radius 0.32
Time taken for clustering: 31.38017696607858
Number of clusters: 7216
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.8104910850524902e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.324764622613
Adjusted Mutual Information: 0.296752405862
Completeness (recall): 0.63373173994
Homogeneity (precision): 0.962971679205
V-score:  0.764407103358
Get results with radius 0.33
Time taken for clustering: 31.64625237416476
Number of clusters: 6925
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.716427505016327e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.439202551651
Adjusted Mutual Information: 0.400702143818
Completeness (recall): 0.687248944399
Homogeneity (precision): 0.915030333385
V-score:  0.784948840606
Get results with radius 0.34
Time taken for clustering: 30.537843968719244
Number of clusters: 6567
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.794658601284027e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.475202096842
Adjusted Mutual Information: 0.449016241786
Completeness (recall): 0.711746860985
Homogeneity (precision): 0.898443015961
V-score:  0.794271539698
Get results with radius 0.35
Time taken for clustering: 31.93396243825555
Number of clusters: 6168
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.7369166016578674e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.538077340117
Adjusted Mutual Information: 0.546639607502
Completeness (recall): 0.762249447753
Homogeneity (precision): 0.856410451406
V-score:  0.80659117332
Get results with radius 0.36
Time taken for clustering: 32.23299645073712
Number of clusters: 5751
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.7564743757247925e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.629452686286
Adjusted Mutual Information: 0.655818550186
Completeness (recall): 0.843540257281
Homogeneity (precision): 0.814153509264
V-score:  0.828586406646
Get results with radius 0.37
Time taken for clustering: 30.924160107038915
Number of clusters: 5366
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.519918441772461e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.678589229664
Adjusted Mutual Information: 0.657141225334
Completeness (recall): 0.893081272578
Homogeneity (precision): 0.804343629994
V-score:  0.846392946842
Get results with radius 0.38
Time taken for clustering: 31.371616343967617
Number of clusters: 4919
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.8421560525894165e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.685506931035
Adjusted Mutual Information: 0.640181037672
Completeness (recall): 0.918321034855
Homogeneity (precision): 0.786969367272
V-score:  0.847586455481
Get results with radius 0.39
Time taken for clustering: 31.50498573947698
Number of clusters: 4496
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.8058344721794128e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.638582021103
Adjusted Mutual Information: 0.564386689469
Completeness (recall): 0.93112666066
Homogeneity (precision): 0.725687771532
V-score:  0.815670383188
Get results with radius 0.4
Time taken for clustering: 31.783488154411316
Number of clusters: 3989
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.712702214717865e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.591613367115
Adjusted Mutual Information: 0.505080132642
Completeness (recall): 0.953313970355
Homogeneity (precision): 0.670217488703
V-score:  0.787083849336
Get results with radius 0.41
Time taken for clustering: 32.25779774971306
Number of clusters: 3506
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.562759280204773e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.529543368972
Adjusted Mutual Information: 0.439471558412
Completeness (recall): 0.948975887181
Homogeneity (precision): 0.610444879925
V-score:  0.742964931238
Get results with radius 0.42
Time taken for clustering: 32.126036858186126
Number of clusters: 3002
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.9706785678863525e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.517629996611
Adjusted Mutual Information: 0.418510368132
Completeness (recall): 0.972634376274
Homogeneity (precision): 0.5839322393
V-score:  0.729750418229
Get results with radius 0.43
Time taken for clustering: 33.10868980269879
Number of clusters: 2563
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.6316771507263184e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.437712754333
Adjusted Mutual Information: 0.33918504077
Completeness (recall): 0.983736508554
Homogeneity (precision): 0.497155356164
V-score:  0.660507206412
Get results with radius 0.44
Time taken for clustering: 31.80538995191455
Number of clusters: 2141
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.6195699572563171e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.376495036995
Adjusted Mutual Information: 0.282353404156
Completeness (recall): 1.0
Homogeneity (precision): 0.427073507485
V-score:  0.598530496495
Get results with radius 0.45
Time taken for clustering: 33.06682531349361
Number of clusters: 1704
Number of faces in total: 8271
Time taken for calculating the distance matrix: 1.687556505203247e-06
Num of files in truefaces is 269
Num of files in face_db.db is 269
Adjusted Rand index: 0.326618925074
Adjusted Mutual Information: 0.240438026987
Completeness (recall): 1.0
Homogeneity (precision): 0.374592267746
V-score:  0.545023097445
DONE!!!
