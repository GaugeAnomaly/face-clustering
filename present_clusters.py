import os, shutil
import numpy as np
from face_models import mod_sqlite3
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-b', '--data_base', help='The SQLite database file to store information', default='./face_db.db')
parser.add_argument('-o', '--output_folder', help='Path to the output folder', default='output_folder/')
args = parser.parse_args()

con = mod_sqlite3.connect(args.data_base, detect_types=mod_sqlite3.PARSE_DECLTYPES)
chips, labels = list(zip(*con.execute('select chip, cluster_id from face')))

if os.path.isdir(args.output_folder):
    shutil.rmtree(args.output_folder)

print('Num of clusters for p:', len(set(labels)))

for i in set(labels):
    class_folder_path = os.path.join(args.output_folder, str(i))
    # Ensure output directory exists
    if not os.path.isdir(class_folder_path):
        os.makedirs(class_folder_path)

    # Save the extracted faces
    for index in np.nonzero(np.asarray(labels) == i)[0]:
        file_path = os.path.join(class_folder_path, "face_" + str(index))
        shutil.copyfile(chips[index], file_path)